import 'package:flutter/material.dart';
import 'package:yes_no_app/config/helpers/get_yes_no_answers_app.dart';
import 'package:yes_no_app/domain/entities/Message.dart';

class ChatProvider extends ChangeNotifier {
  final ScrollController chatScrollController = ScrollController();
  final GetYesNoAnswers getYesNoAnswers = GetYesNoAnswers();

  List<Message> messageList = [
    Message(text: "Hola, como estas?", fromWho: FromWho.me),
    Message(text: "Ya, regresaste del trabajo?", fromWho: FromWho.me),
  ];

  Future<void> herReply() async {
    final herMessage = await getYesNoAnswers.getAnswer();

    messageList.add(herMessage);
    notifyListeners();
    moveScrollToBotton();
  }

  Future<void> addMessage(String text) async {
    if(text.isEmpty) {
      return;
    }

    final newMessage = Message(text: text, fromWho: FromWho.me);
    messageList.add(newMessage);

    notifyListeners();
    moveScrollToBotton();

    if( text.endsWith('?') ) {
      await Future.delayed(const Duration(milliseconds: 1000));
      await herReply();
    }

    notifyListeners();
    moveScrollToBotton();
  }

  Future<void> moveScrollToBotton () async {
    await Future.delayed(const Duration(milliseconds: 100));

    chatScrollController.animateTo(
      chatScrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeInOut,
    );
  }
}