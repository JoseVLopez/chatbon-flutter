import "package:flutter/material.dart";

class MessageFieldBox extends StatelessWidget {
    final ValueChanged<String> onValue;

  const MessageFieldBox(
    {
      super.key,
      required this.onValue,
    }
  );

  @override
  Widget build(BuildContext context) {
    final OutlineInputBorder border = OutlineInputBorder(
      borderSide: const BorderSide(color: Colors.transparent),
      borderRadius: BorderRadius.circular(40),
    );

    final textController = TextEditingController();

    final focusNode = FocusNode();

    final InputDecoration inputDecoration = InputDecoration(
      hintText: 'End your message "?"',
      enabledBorder: border,
      focusedBorder: border,
      border: const UnderlineInputBorder(),
      filled: true,
      suffixIcon: IconButton(
        icon: const Icon( Icons.send_outlined ),
        onPressed: () {
          onValue(textController.text);
          textController.clear();
        },
      ),
    );

    return TextFormField(
      onTapOutside: (event) {
        focusNode.unfocus();
      
      },
      focusNode: focusNode,
      controller: textController,
      decoration: inputDecoration,
      onFieldSubmitted: (value) => {
          textController.clear(),
          focusNode.requestFocus(),
          onValue(value),
      },
    );
  }
}